.DELETE_ON_ERROR:

sources      != find contents -type f -name '*.sh'
annexfiles   != find contents -type f -not -name '*.sh'
layouts      != find layouts  -type f

pages         = ${sources:contents/%.sh=public/%.html}
annexrules    = ${annexfiles:contents/%=public/%}

all: exec public/articles.tsv ${pages} ${annexrules}

test:;
	busybox httpd -p 1312 -h public
	@ echo "http://localhost:1312"
stop:; ps -aux | grep 'busybox httpd .*-p.*1312' | grep -v 'grep' | awk '{print $$2}' | xargs -r kill

clean:; rm -r public/*

exec:; chmod +x ${sources}

public/articles.tsv: maketsv contents/actualites/*
	@mkdir -p $(shell dirname $@)
	./$< > $@

public/%.html              : contents/%.sh page ${layouts}
	@mkdir -p $(shell dirname $@)
	$< > $@
public/%                   : contents/%
	@mkdir -p $(shell dirname $@)
	cp $< $@

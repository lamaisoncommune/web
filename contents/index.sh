#! /usr/bin/env ./page
title: La Maison Commune
author: la Maison Commune
description: "La maison commune est une maison proposant de l'hébergement d'urgence et de long terme pour les personnes exilées à Calais"

section: main

## La maison

Située à Calais, dans les Hauts-de-France, **la Maison Commune** est une maison 
accueillante dédiée à proposer un hébergement et un soutien aux **femmes et 
familles en situation d’exil** sur le littoral nord. Un projet né du manque de
possibilités de mise à l’abri par les pouvoirs publics, même pour les personnes 
les plus vulnérables. Nous nous efforçons de garantir l'accès **gratuit** aux besoins 
essentiels : dormir dans un lit confortable dans un lieu calme, se reposer, se nourrir,
se laver, accéder aux soins médicaux, se sentir en sécurité et pouvoir communiquer librement.

En été, nous proposons des **hébergements d’une durée de trois nuits**, avec pour 
objectif d'étendre cette aide toute l'année pour offrir un abri digne, sûr, et un
soutien plus continu.

Ensemble, luttons contre la violence des politiques frontalières françaises
et européennes, et oeuvrons pour un monde plus juste et solidaire, 

> **Personne n’est illégal**\
> **لا يوجد انسان غير شرعي**\
> **«Il n’y a pas de crise migratoire, seulement une crise de l’accueil»**


## L'équipe

![L'équipe](/equipe.webp)

## Nous contacter

Par mail : maisoncommunecalais@gmail.com

Sur instagram : [@lamaisoncommunecalais](https://www.instagram.com/lamaisoncommunecalais/)


endsection

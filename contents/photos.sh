#! /usr/bin/env ./page
title: Exemple de galleries
author: Arthur
description: "Un exemple de gallerie"

section: main

## Photos :)

La grande chambre :

endsection

gallerie: main

/equipe.webp "Cute cate"
/moyenne-maison.webp "machin"
/petite-maison.webp "machin"

endgallerie

section: main

Et la petite :

endsection

gallerie: main

/petite-maison.webp "machin"
/moyenne-maison.webp "machin"
/equipe.webp "Cute cate"

endgallerie

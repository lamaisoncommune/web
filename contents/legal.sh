#! /usr/bin/env ./page
title: Mentions légales
author: la Maison Commune
description: "Mentions légales"

section: main

Conformément aux dispositions des articles 6-III et 19 de la Loi n° 2004-575 du
21 juin 2004 pour la Confiance dans l’économie numérique, dite L.C.E.N., nous
portons à la connaissance des utilisateurs et visiteurs du site
lamaisoncommunecalais.fr les informations suivantes.

## Éditeur du site

### La Maison Commune

Association loi 1901 déclarée en sous-préfecture de Dunkerque le 22 avril 2024.

Siège social :\
terre plein du Jeu de Mail\
Rue du 11 Novembre 1918\
La bourse des Associations\
59140 Dunkerque

RNA: W594013223

Pour nous contacter : maisoncommunecalais@gmail.com

## Hébergement

Le site est hébergé via le service pages de l'instance gitlab de framasoft :
https://framagit.org. \
D'après [leurs mentions légales](https://framasoft.org/fr/legals/) le
service est possiblement hébergé par :

Hetzner Online GmbH\
Industriestr. 25\
91710 Gunzenhausen\
Germany

Tél.: +49 (0)9831 505-0

## Contenus

Sauf mention contraire, tous les textes et contenus présentés sont mis à
disposition selon les termes de la license ...

## Design et développement web

Design du site : Alice Poulain\
Développement du site et du générateur : Arthur Pons et Marc Chantreux\
CSS : Pauline

Générateur du site : [Francium](https://git.unistra.fr/arthur.pons/francium)


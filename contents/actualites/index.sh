#! /usr/bin/env ./page
title: Les actualités
author: la maison commune
description: "Les actualités de la maison commune"

section: main
La liste de nos actus par ordre chronologique

------
endsection

#/actualites/le-debut/index.html	la maison commune	C'est parti pour le projet de rénovation de la maison commune !	2024-03-01	Le début !

cat public/articles.tsv |
	sort -t'	' -k4 -n |
	awk -F'\t' -v today=$(date +'%Y-%m-%d') '{if($4<=today){print $0}}' |
	printlayout ./layouts/listearticles lien auteurice description date titre |
	save_md main

